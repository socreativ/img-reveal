<?php
    
$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$image = get_field('image');
$alignement = get_field('position');

?>  

<?php if(!my_wp_is_mobile()): ?>
    <section id="<?= $id ?>" class="img-reveal-container <?= $alignement ?> <?= $css ?>">
        <div class="position-relative image-reveal">
            <div class="cover-reveal" data-bkg="<?= $image['url']; ?>"></div>
        </div>
    </section>
<?php endif; ?>


