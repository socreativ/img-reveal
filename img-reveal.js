"use strict"
docReady(() => {

    const imgReveal = Array.from(document.querySelectorAll('.img-reveal-container'));
    imgReveal.forEach(element => {
        const img = element.children[0].children[0];
        const frames = 17;
        const bkg = img.getAttribute('data-bkg');

        ScrollTrigger.create({
            trigger: element,
            onUpdate: ({direction, progress}) => {
                const progressRound = Math.round(frames * progress) / frames;
                const percent = progressRound*100;
                img.style.backgroundImage = `url("${bkg}")`
                img.style.WebkitMaskPosition = `${percent}% 50%`;
                img.style.maskPosition = `${percent}% 50%`;
                
            },
            start: "top 80%",
            end: "bottom top",
        });        


        // PIN
        ScrollTrigger.create({
            trigger: element,
            pin: element,
            start: "top top",
            end: "bottom center",
        });

        // APPEAR
        ScrollTrigger.create({
            trigger: element,
            start: 'top 90%',
            end: 'bottom top+=25%',
            onEnter: () => gsap.to(element, {
                opacity: 1,
                stagger: 0.2,
            }),
            onLeave: () => gsap.to(element, {
                opacity: 0,
                stagger: 0.2,
            }),
            onEnterBack: () => gsap.to(element, {
                opacity: 1,
                stagger: -0.2,
            }),
            onLeaveBack: () => gsap.to(element, {
                opacity: 0,
                stagger: -0.2,
            }),
            });
    });     

    }
);

            
      
            

          
  
